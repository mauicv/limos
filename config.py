"""Config."""
import os

from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)


class App_Param():
    """Config application variables."""
    UPDATE_RATE = os.environ.get('UPDATE_RATE')
    TOKEN = os.environ.get('TOKEN')


class Config(object):
    """Config for flask."""
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    MAIL_DEFAULT_SENDER = os.environ.get('MAIL_DEFAULT_SENDER')
    MAIL_USE_TLS = True
    if os.getenv('APP_SETTINGS') == 'production':
        SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")
        TESTING = False
        DEBUG = False
    elif os.getenv('APP_SETTINGS') == 'development':
        SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")
        TESTING = True
        DEBUG = True
    elif os.getenv('APP_SETTINGS') == 'testing':
        SQLALCHEMY_DATABASE_URI = os.environ.get("TEST_DATABASE_URI")
        TESTING = True
        DEBUG = True
