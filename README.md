### Limos:
---
limos is a data monitoring server. Users should set it up on a remote machine and create scheduled feeder events that run either locally or remotely and send data to the server. In doing so the api with update the feed val in the database.

The api accepts http requests that transfer numeric vals to specified feed vals in the database.

On receiving a numeric val the api will proceed to compute the new average and new variance from the new sample point.

As well as this it will monitor for data point outliers that lie greater than some distance from the feed's mean. typically 3 times the standard deviation.

time_stamp in form `'%Y-%m-%d %H:%M:%S'`
