from feeders.iex import iex
from config import App_Param


def seed_db():
    feeder = iex(App_Param.TOKEN)
    # feeder._init_seed()
    feeder._init_seed()


def run_update():
    feeder = iex(App_Param.TOKEN)
    feeder._run_update()
