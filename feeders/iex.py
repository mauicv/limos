import requests
from feeders import feed_core
# from config import App_Param
from datetime import datetime
import json


class iex(feed_core):
    """iex feeder class for scripts."""

    def __init__(self, token):
        super().__init__(token, 'iex')
        self._get_data_map()
        self.base_url = 'https://api.iextrading.com/1.0'

    def _get_iex_sym_name(self, symbol_datum):
        return symbol_datum.get('name') if \
            symbol_datum.get('name') != '' \
            else symbol_datum.get('symbol')

    def _sync_data_map(self):
        sym_r = requests.get(self.base_url+'/ref-data/symbols')
        symbol_data = json.loads(sym_r.text)
        for index, symbol_datum in enumerate(symbol_data):
            if symbol_datum['symbol'] not in self.map:
                self._post_new_iex_feed(symbol_datum)

    def _post_new_iex_feed(self, symbol_datum):
        name = self._get_iex_sym_name(symbol_datum)
        post_data = {
            'name': name,
            'token': self.key,
            'time_units': 'days'
        }
        r = requests.post('http://localhost:5000/new_feed',
                          json=post_data)
        db_data = json.loads(r.text)
        self.map[symbol_datum['symbol']] = {
                'id': db_data['id'],
                'name': symbol_datum['name'],
            }
        self._save_data_map()

    def _init_seed(self):
        self._sync_data_map()
        for symbol in self.map:
            entity_r = requests.get(self.base_url+f'/stock/{symbol}/chart/3m')
            entity_data = json.loads(entity_r.text)
            for hist_datum in entity_data:
                avg = self._get_val(hist_datum)
                if avg:
                    post_data = {
                      'id': self.map.get(symbol)['id'],
                      'val': avg,
                      'time_stamp': self._parse_date(hist_datum['date']),
                      'token': self.key,
                    }
                    requests.post('http://localhost:5000/data',
                                  json=post_data)
            self._save_data_map()

    def _check_in_db(self, code):
        if self.map.get(code, None):
            return True
        else:
            return False

    def _get_val(self, dict):
        if dict.get('high', None) and dict.get('high', None):
            return (dict['high'] + dict['low'])/2
        else:
            return False

    def _run_update(self):
        for key, map in self.map.items():
            entity_r = requests.get(self.base_url+f'/stock/{key}/ohlc')
            entity_data = json.loads(entity_r.text)
            val = self._get_val(entity_data)

            if val:
                post_data = {
                  'id': map['id'],
                  'val': val,
                  'time_stamp': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                  'token': self.key,
                }
                requests.post('http://localhost:5000/data', json=post_data)
            else:
                # at some point it will make sense to keep track of the
                # data that is bad. Can even plug these numbers into seperate
                # feeds.
                print('missing or incorrect val')
