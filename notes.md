### Notes:

The idea is consistent measurements of small to medium numbers of significant data feeds. At its core it will be an online data pipeline. Because it's online and the main time-wise constraint is the fetching of relevant data from apis and online data sources we can relax on the typical data analysis aims, Namely speed. Due to this the idea is to learn as much as possible from a slow but timely collection of significant data sources.

Long term the speed considerations might be mitigated somewhat by having collections of limos nodes. The current model is limos core and a script that runs and collects data, after which it internally posts that data to limos core. In scaling these ideas one could build the limos network of nodes that funnel and filter data much in the way a neural network would.

Not clear how best to implement time differences in updating the feeds. Do we consider the time provided in the update vals or the local time? I like local time because it simplifies a lot of considerations, except in the case of wanting to look into the past.

we've implemented both because it's simple to do so without a lot of difficulty.
