import json
import decimal

from flask import jsonify, request
from app import app
import datetime

from app import db
from app.models import Feed, Event
from app.auth import requires_auth
# from config import App_Param
from flask import render_template
from sqlalchemy import and_
from app.analytics.formatter import Formatter
from app.analytics.stats import Stats


@app.route('/', methods=['GET'])
# @requires_auth
def stats():
    today = datetime.datetime.today()
    date_range = [
        today - datetime.timedelta(days=1),
        today
    ]

    events = db.session.query(Event) \
        .filter(
            and_(Event.time_stamp >= str(date_range[0]),
                 Event.time_stamp <= str(date_range[1]))).all()

    events_formatter = Formatter(events)
    statistics = Stats(events)

    return render_template('stats.html',
                           title='stats',
                           data=events_formatter.format(),
                           meta=statistics.get_meta(),
                           date=today.strftime("%Y-%m-%d"))


@app.route('/data', methods=['POST'])
@requires_auth
def data():
    data = json.loads(request.data)
    if not data.get('val', None) or not data.get('id', None):
        return 'No val or id provided!', 422

    feed = db.session.query(Feed) \
        .filter_by(id=data.get("id")).first()
    if not feed:
        return 'No feed corresponds to id!', 404
    if feed.update(data):
        feed.check_for_event(decimal.Decimal(float(data.get("val"))))
        return jsonify(message="feed updated")
    else:
        return 'Error updating feed!', 501


@app.route('/events_since/<duration>', methods=['POST'])
@requires_auth
def events_since(duration):
    events = []
    today = datetime.datetime.today()
    date_range = [
        today - datetime.timedelta(days=int(duration)),
        today
    ]

    events_since = db.session.query(Event) \
        .filter(
            and_(Event.time_stamp >= str(date_range[0]),
                 Event.time_stamp <= str(date_range[1]))).all()

    if not events_since:
        return 'No events have occured in that time frame', 404
    for event in events_since:
        events.append({
                'time_stamp': event.time_stamp,
                'name': event.name,
                'feed_id': event.feed_id,
                'sig': float(event.sig),
                'avg': float(event.avg),
                'dev': float(event.dev),
                'val': float(event.val),
            })
    return jsonify(data=events)


@app.route('/events/<id>', methods=['POST'])
@requires_auth
def events(id):
    events = []
    feed_events = db.session.query(Event) \
        .filter_by(feed_id=id).all()
    if not feed_events:
        return 'That feed has no assoicated events', 404
    for feed_event in feed_events:
        events.append({
                'time_stamp': feed_event.time_stamp,
                'name': feed_event.name,
                'feed_id': feed_event.feed_id,
                'sig': float(feed_event.sig),
                'avg': float(feed_event.avg),
                'dev': float(feed_event.dev),
                'val': float(feed_event.val),
            })
    return jsonify(data=events)


@app.route('/feeds', methods=['POST'])
@requires_auth
def feeds():
    events = []
    feeds = db.session.query(Feed).all()
    for feed in feeds:
        events.append({
                'id': feed.id,
                'time_stamp': feed.time_stamp,
                'name': feed.name,
                'avg': float(feed.avg) if feed.avg else 'None',
                'var': float(feed.var) if feed.var else 'None',
            })
    return jsonify(data=events)


@app.route('/new_feed', methods=['POST'])
@requires_auth
def new_feed():
    data = json.loads(request.data)
    # try:
    print(data)
    type = data.get('type') if data.get('type', False) else 'unknown'

    feed = Feed(name=data['name'],
                time_units=data['time_units'],
                type=type)
    db.session.add(feed)
    db.session.commit()
    return jsonify(id=feed.id, name=feed.name)
    # except:
    #     return jsonify(message="Something went wrong")
