"""Task schedule."""
import datetime
# from config import Config
from app.models import Feed, Association
from app import db

from graph_tool.all import Graph, load_graph


class Mapper:
    """Class to map data between the db and the api."""

    #  This is untested...

    def __init__(self):
        self.g = Graph()
        v_data = self.g.new_vertex_property("object")
        e_weight = self.g.new_edge_property("double")

        # this seems to be needed in order to save graph...
        self.g.edge_properties["weight"] = e_weight
        self.g.vertex_properties["data"] = v_data

        feeds = db.session.query(Feed).all()
        links = db.session.query(Association).all()

        link_dict = {}

        for feed in feeds:
            v = self.g.add_vertex()
            link_dict[feed.id] = self.g.vertex_index[v]
            v_data[v] = {'name': feed.name}

        for link in links:
            v_1 = self.g.vertex(link_dict[link.left_feed.id])
            v_2 = self.g.vertex(link_dict[link.right_feed.id])
            e = self.g.add_edge(v_1, v_2)
            e_weight[e] = link.weight

    def save(self):
        date_str = datetime.datetime.now().strftime("%Y-%m-%d")
        self.g.save(date_str + ".xml.gz")

    def load(self, time):
        if not time:
            date_str = datetime.datetime.now().strftime("%Y-%m-%d")
        else:
            date_str = time.strftime("%Y-%m-%d")
        self.g = load_graph(date_str + ".xml.gz")
