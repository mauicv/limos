"""Task schedule."""
# import datetime
# from config import Config


class Formatter:
    """Class to format data for either frontend or email."""

    def __init__(self, events):
        self.events = [{
            'dev': round(event.dev, 2),
            'avg': round(event.avg, 2),
            'val': round(event.val, 2),
            'sig': round(event.sig, 2),
            'feed': {'name': event.feed.name}
        } for event in events]
        self.order_events()

    def order_events(self):
        self.events.sort(key=lambda event: event['sig'], reverse=True)

    def format(self):
        return self.events
