"""Task schedule."""
# import datetime
# from config import Config


class Stats:
    """Class to format data for either frontend or email."""

    def __init__(self, events):
        self.events = events

    def get_meta(self):
        meta = {
            'total_number_events': len(self.events)
        }
        return meta
