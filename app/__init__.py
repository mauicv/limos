from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
mail = Mail(app)

from app import routes

"""Background Processes"""
from app.tasks.email_update import email_update
from app.tasks.data_update import data_update

from apscheduler.schedulers.background import BackgroundScheduler
import atexit

scheduler = BackgroundScheduler()
scheduler.add_job(email_update,
                  "cron",
                  hour=7,
                  minute=30)
scheduler.add_job(data_update,
                  "cron",
                  hour=0)
scheduler.start()
# Shut down the scheduler when exiting the app
atexit.register(lambda: scheduler.shutdown())
