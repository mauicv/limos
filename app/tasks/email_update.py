"""Task schedule."""
import datetime
# from jinja2 import Template
from jinja2 import Environment, FileSystemLoader

from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker

from app.models import Event
from config import Config
from app import app
from app import mail
from flask_mail import Message

from app.analytics.formatter import Formatter
from app.analytics.stats import Stats


engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)

file_loader = FileSystemLoader('./app/templates')
env = Environment(loader=file_loader)


class db_conn:
    """Manage db connection for background task."""
    session_maker = sessionmaker(bind=engine)

    def __init__(self):
        self.session = None

    def __enter__(self):
        self.session = db_conn.session_maker()
        return self.session

    def __exit__(self, type, val, traceback):
        self.session.close()


def email_update():
    """Periodically runs and sends email if significant outlier is found."""
    with db_conn() as session:
        today = datetime.datetime.today()
        date_range = [
            today - datetime.timedelta(days=1),
            today
        ]

        events = session.query(Event) \
            .filter(
                and_(Event.time_stamp >= str(date_range[0]),
                     Event.time_stamp <= str(date_range[1]))).all()

        events_formatter = Formatter(events)
        statistics = Stats(events)

        template = env.get_template('stats.html')

        if events:
            subject = "Event Log"
            with app.app_context():
                msg = Message(subject=subject,
                              recipients=["a.thornysort@gmail.com"])

                msg.html = template.render(title='stats',
                                           data=events_formatter.format(),
                                           meta=statistics.get_meta(),
                                           date=today.strftime("%Y-%m-%d"))

                mail.send(msg)
