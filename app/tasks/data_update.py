from app.tasks.update_scripts.iex import iex
from app.tasks.update_scripts.internal import internal
from config import App_Param

import datetime
from app import db
from app.models import Event
from sqlalchemy import and_


def data_update():
    iex_updater = iex(App_Param.TOKEN)
    iex_updater._run_update()

    internal_updater = internal(App_Param.TOKEN)
    internal_updater._run_update()

    today = datetime.datetime.today()
    date_range = [
        today - datetime.timedelta(days=1),
        today
    ]

    events = db.session.query(Event) \
        .filter(
            and_(Event.time_stamp >= str(date_range[0]),
                 Event.time_stamp <= str(date_range[1]))).all()

    for event in events:
        event.update_links()
