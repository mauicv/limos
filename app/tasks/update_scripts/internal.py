import requests
from datetime import datetime
import json

from app.tasks.update_scripts import feed_core
# from config import App_Param


class internal(feed_core):
    """iex feeder class for scripts."""

    def __init__(self, token):
        super().__init__(token, 'internal')
        self._get_data_map()
        self.events_today = []
        if self.map['seeded'] is False:
            post_data = {
                'name': 'Number Of Events',
                'token': self.key,
                'time_units': 'days',
                'type': 'internal'
            }
            r = requests.post('http://localhost:5000/new_feed',
                              json=post_data)
            db_data = json.loads(r.text)
            self.map['number_of_events'] = {
                    'id': db_data['id'],
                    'name': 'Number Of Events',
                }
            self.map['seeded'] = True
            self._save_data_map()

    def _run_update(self):
        post_data = {
            'token': self.key,
        }
        r = requests.post('http://localhost:5000/events_since/1',
                          json=post_data)
        self.events_today = json.loads(r.text)['data']
        self.number_events_today = len(self.events_today)

        if self.number_events_today:
            post_data = {
              'id': self.map['number_of_events']['id'],
              'val': self.number_events_today,
              'time_stamp': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
              'token': self.key,
            }
            requests.post('http://localhost:5000/data', json=post_data)
