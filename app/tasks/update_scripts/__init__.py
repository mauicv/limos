import requests
import json
import datetime


class feed_core:
    @staticmethod
    def _parse_date(date):
        return datetime.datetime.strptime(date, '%Y-%m-%d') \
            .strftime("%Y-%m-%d %H:%M:%S")

    def __init__(self, token, name):
        self.key = token
        self.feeds = self._get_feeds()
        self.name = name

    def _make_feed(self, name, time_units, time):
        r = requests.post('http://localhost:5000/new_feed',
                          json={
                            'name': name,
                            'time_stamp': time,
                            'token': self.key,
                            'time_units': time_units
                          })
        self.feeds.append(json.loads(r.text))
        print('status_code: ', r.status_code)

    def _get_feeds(self):
        r = requests.post('http://localhost:5000/feeds',
                          json={
                             'token': self.key
                          })
        return json.loads(r.text)['data']

    def _get_data_map(self):
        try:
            with open(self.name+'_maps.txt', 'r') as file:
                self.map = json.loads(file.read())
        except:
            self.map = {
                'seeded': False
            }

    def _save_data_map(self):
        with open(self.name+'_maps.txt', 'w') as file:
            file.write(json.dumps(self.map, indent=4, separators=(',', ': ')))
