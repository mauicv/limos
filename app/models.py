"""Database models."""
import datetime
import math
import decimal

from app import db
from sqlalchemy import Column, ForeignKey, and_
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import INTEGER, \
    NUMERIC, TIMESTAMP, TEXT, JSONB

from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.ext.associationproxy import association_proxy


class Feed(db.Model):
    """Records avg and variance of a data stream."""

    __tablename__ = 'feed'
    id = Column(INTEGER, primary_key=True)
    name = Column(TEXT)
    type = Column(TEXT)
    created_at = Column(TIMESTAMP,
                        nullable=False,
                        default=datetime.datetime.utcnow)

    time_stamp = Column(TIMESTAMP)
    total_time = Column(NUMERIC, default=0)
    prev_total_time = Column(NUMERIC, default=0)
    time_units = Column(TEXT)
    avg = Column(NUMERIC)
    var = Column(NUMERIC, default=0)
    # small alpha corresponds to ema over longer periods
    alpha = Column(NUMERIC, default=0.01)
    sen = Column(NUMERIC, default=1.96)
    meta = Column(JSONB)
    sample_size = Column(NUMERIC, default=0)

    events = relationship("Event", backref="feed")

    to_feeds = association_proxy('to_links', 'right_feed')
    from_feeds = association_proxy('from_links', 'left_feed')

    def __repr__(self):
        """Instance -> str."""
        return '<Feed id: {}>'.format(self.id)

    @hybrid_property
    def std_dev(self):
        return decimal.Decimal(math.sqrt(self.var))

    def _time_diff(self, time):
        if self.time_stamp:
            last_time = self.time_stamp
            this_time = datetime.datetime.strptime(time, '%Y-%m-%d %H:%M:%S')
            return getattr(this_time - last_time, self.time_units)
        else:
            return None

    def _time_update(self, time):
        self.time_stamp = time
        db.session.add(self)
        db.session.commit()

    def update(self, payl):
        time = payl.get('time_stamp',
                        datetime.datetime.utcnow()
                        .strftime('%Y-%m-%d %H:%M:%S'))
        time_diff = self._time_diff(time)
        if time_diff is None or time_diff >= 1:
            self._time_update(time)
            self.sample_size += 1
            return self._update_data(payl['val'])
        else:
            return False

    def _update_data(self, val):
        if self.avg is None:
            self.avg = val
            db.session.add(self)
            db.session.commit()
        else:
            # try:
            diff = decimal.Decimal(val) - self.avg
            incr = self.alpha * diff
            self.avg = self.avg + incr
            self.var = (1 - self.alpha) * (self.var + diff * incr)
            db.session.add(self)
            db.session.commit()
            return True
            # except:
            #     return False

    def _create_event(self, val):
        event = Event(val=val,
                      avg=self.avg,
                      dev=self.std_dev,
                      feed_id=self.id,
                      time_stamp=self.time_stamp,
                      sen=self.sen)
        db.session.add(event)
        db.session.commit()

    def check_for_event(self, val):
        print(self._out_dist(val))
        if self._out_dist(val) > 0:
            self._create_event(val)
        else:
            return None

    def _out_dist(self, val):
        if val >= self.avg+(self.sen*self.std_dev):
            return val-self.avg+(self.sen*self.std_dev)
        elif val <= self.avg-(self.sen*self.std_dev):
            return self.avg-(self.sen*self.std_dev)-val
        else:
            return False

    def linked_to(self, feed):
        return feed in self.to_feeds

    def get_link(self, feed):
        if self.linked_to(feed):
            link = next(link for link in self.to_links
                        if link.left_feed == self
                        and link.right_feed == feed)
            return link
        else:
            return False

    def create_link_to(self, feed):
        link = Association(left_feed=self,
                           right_feed=feed)
        db.session.add(link)
        db.session.commit()

    def bump_link_to(self, feed):
        link = self.get_link(feed)
        link.bump(feed.time_stamp)


class Association(db.Model):
    __tablename__ = 'association'
    id = Column(INTEGER, primary_key=True)
    left_feed_id = Column(INTEGER, ForeignKey('feed.id'))
    right_feed_id = Column(INTEGER, ForeignKey('feed.id'))
    count = Column(NUMERIC, default=1)
    meta = Column(JSONB)
    updated_at = Column(TIMESTAMP)

    left_feed = db.relationship(Feed,
                                primaryjoin=(left_feed_id == Feed.id),
                                backref='to_links')

    right_feed = db.relationship(Feed,
                                 primaryjoin=(right_feed_id == Feed.id),
                                 backref='from_links')

    @hybrid_property
    def weight(self):
        sum = 0
        for link in self.left_feed.to_links:
            sum = sum + link.count
        return self.count/sum

    def bump(self, time):
        if self.updated_at:
            if getattr(time - self.updated_at, 'days') > 1:
                self.count += 1
        else:
            self.updated_at = time
            self.count += 1
        db.session.add(self)
        db.session.commit()


class Event(db.Model):
    """Records outlier events.

    In our case we define an outlier as an event that occurs with a val
    greater than alpha times the distance from the mean.
    """

    __tablename__ = 'event'
    id = Column(INTEGER, primary_key=True)
    name = Column(TEXT)
    time_stamp = Column(TIMESTAMP,
                        nullable=False,
                        default=datetime.datetime.utcnow)

    avg = Column(NUMERIC)
    val = Column(NUMERIC)
    dev = Column(NUMERIC)
    sen = Column(NUMERIC, default=1.96)
    feed_id = Column(INTEGER, ForeignKey('feed.id'))
    meta = Column(JSONB)

    @hybrid_property
    def sig(self):
        return abs(self.val - self.avg)/(self.dev*self.sen)

    def _out_dist(self):
        if self.val >= self.avg+(self.sen*self.dev):
            return self.val-(self.avg+(self.sen*self.dev))
        elif self.val <= self.avg-(self.sen*self.dev):
            return self.avg-(self.sen*self.dev)-self.val
        else:
            return False

    def get_close_events(self):
        date_range = [
            self.time_stamp - datetime.timedelta(days=0.5),
            self.time_stamp + datetime.timedelta(days=0.5)
        ]

        events = db.session.query(Event) \
            .filter(
                and_(Event.time_stamp >= str(date_range[0]),
                     Event.time_stamp <= str(date_range[1]))).all()

        return events

    def update_links(self):
        '''Should run through all recent events to update assoication table.'''
        close_events = self.get_close_events()
        for event in close_events:
            if event.feed is not self.feed:
                if self.feed.linked_to(event.feed):
                    self.feed.bump_link_to(event.feed)
                else:
                    self.feed.create_link_to(event.feed)
