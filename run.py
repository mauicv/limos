"""Runs the application."""

import os

from app import app

host = '0.0.0.0'
port = 5000

if __name__ == '__main__':
    app.run(host=host, port=port)
