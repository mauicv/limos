### TO DO:

- [ ] TESTING: Write the runner.py file so that we can use it to generate and property
  - [ ] create simple environments.
  - [ ] create moving environments.
  - [ ] test event link association
  - [ ] test graph creation, saving and load.


- [ ] should be able to filter feeds by type on post request. So url: `feeds/<type>` This way when we runt he update script we don't need to store the data_map txt file we just get by type and use any associated information in JSONB string to run updates.
- [ ] Write sync script that checks each feed is still active.
- [ ] Number of events for each outlier.
- [ ] Reoccurrence of event. Time till last...
- [ ] Add var and std_dev to tests... Would have caught std_dev issue sooner if We had already implemented this!
- [ ] The model layer logic is becoming quite complicated should split into separate files.
- [ ] Associations should be dependent on time-wise resolution
  - if we have two events on at a resolution seconds and the other at days which to we choose.

### TO DO BIG:

- [ ] Testing...
- [ ] implement association network update task
- [ ] refactor `data_map.txt` into database.
  - [ ] `feeds/<type>`


## DONE:

- [x] Reseed database. Note that the db is fucked because of the dev issue...
  - [x] Change significance -> 1.98 ~ 5% of events ?
  - [x] Add association table from feed to itself. Easier to do now than later.
  - [x] Add JSONB column for data associated to specific feeds. Ticker symbol, market type, associated feeds, and so on...
- [x] Number of outliers
- [x] outliers in order of Significance
- [x] We render website same way as alltalk.
- [x] Write sync script that checks new feeds.
- [x] Fix email layout

## useful links:
- Descriptors: https://docs.sqlalchemy.org/en/latest/orm/mapped_attributes.html#using-descriptors-and-hybrids
- Exp moving avg and var: http://people.ds.cam.ac.uk/fanf2/hermes/doc/antiforgery/stats.pdf
- Standard score: https://en.wikipedia.org/wiki/Standard_score

## questions:

- why can't we just run DNA type algos on event collections. As in create an association network from proximity. There should be a negative cost to any two feeds that don't concurrently emit events just as there is a positive one for those that do.

## process involved in graph-tool:

- Follow install process here: https://git.skewed.de/count0/graph-tool/wikis/installation-instructions#debian-ubuntu or here: https://kyzhang.me/2018/02/24/Install-graphtools-on-Ubuntu/
- We won't be able to import graph tool into venv so: https://jolo.xyz/blog/2018/12/07/installing-graph-tool-with-virtualenv
- subsequently Numpy, Scipy might break if you've installed them elsewhere already. In which case i found running `pip install scipy --upgrade` and `pip install numpy --upgrade` seemed to fix the problem.
- there's still some issue to do with ciaro...
