from tests import testUtils


def test_api():
    t = testUtils()
    if t.touch() is not False:
        t.dropAndCreateDb()
        t.test_auth()
        t.make_feeds(10)
        t.get_feeds()
        t.random_updates(5)

    t.dropAndCreateDb()
