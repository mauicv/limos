from tests import testUtils


def test_avg():
    t = testUtils()
    if t.touch():
        # t.dropAndCreateDb()
        id = t.make_feed()['id']

        t.createFakeNumberStream(100, 0)
        t.send_updates(t.fakeNumberStream, id)
        t.get_feeds()

        print('***************************************')
        print('api data')
        print('--------')
        print('     avg: ', t.feeds[0]['avg'])
        # print('     var: ', t.feeds[0]['var'])
        # print('     std_dev: ', math.sqrt(t.feeds[0]['var']))
        print('')
        print('***************************************')
        t.getEvents()
    pass


def create_env(n):
    t = testUtils()
    if t.touch():
        # t.dropAndCreateDb()
        for i in range(n):
            id = t.make_feed()['id']

            t.createFakeNumberStream(100, 0)
            t.send_updates(t.fakeNumberStream, id)
            t.get_feeds()
        t.getEvents()
    pass


def test_associations():
    t = testUtils()
    if t.touch():
        pass
        # t.dropAndCreateDb()
