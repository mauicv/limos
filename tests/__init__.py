import random
import requests
import math
from faker import Faker
from config import App_Param
import json
import datetime

from app import db
import app.models

class testUtils:
    def __init__(self):
        self.key = App_Param.TOKEN
        self.feeds = []
        self.faker = Faker()

        self.fakeNumberStream = []
        self.fakeExpAvg = 0
        self.fakeVar = 0

    def dropAndCreateDb(self):
        db.drop_all()
        db.create_all()

    def touch(self):
        try:
            r=requests.get('http://localhost:5000/')
            return True
        except:
            print('*************************')
            print('Remember to start server!')
            print('*************************')
            return False

    def test_auth(self):
        r = requests.post('http://localhost:5000/new_feed')
        print('expect true: ', r.status_code == 401)
        r = requests.post('http://localhost:5000/data')
        print('expect true: ', r.status_code == 401)
        r = requests.post('http://localhost:5000/feeds')
        print('expect true: ', r.status_code == 401)
        r = requests.post('http://localhost:5000/events/1')
        print('expect true: ', r.status_code == 401)


    def make_feeds(self,n):
        for i in range(0,n):
            name = self.faker.word()
            number = self.faker.random_number()
            r = requests.post('http://localhost:5000/new_feed',
                              json={
                                'name': name,
                                'time_stamp': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                'token': self.key,
                                'time_units': 'seconds'
                                })
            self.feeds.append(json.loads(r.text))
            print('status_code: ',r.status_code)

    def make_feed(self):
        name = self.faker.word()
        r = requests.post('http://localhost:5000/new_feed',
                          json={
                            'name': name,
                            'time_stamp': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                            'token': self.key,
                            'time_units': 'seconds',
                            'type': 'test'
                            })
        feed_val = json.loads(r.text)
        self.feeds.append(feed_val)
        return feed_val

    def get_feeds(self):
        r = requests.post('http://localhost:5000/feeds',
                         json={
                            'token': self.key
                         })
        self.feeds = json.loads(r.text)['data']

    def random_updates(self, n):
        for i in range(0, n):
            for id in [feed['id'] for feed in self.feeds]:
                requests.post('http://localhost:5000/data',
                              json={
                                 'token': self.key,
                                 'val': self.faker.random_number(),
                                 'time_stamp': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                 'id': id
                              })

    def send_updates(self, vals, id):
        for index, val in enumerate(vals):
            time_now = datetime.datetime.now()
            time_stamp = time_now + datetime.timedelta(seconds=index*10)
            requests.post('http://localhost:5000/data',
                          json={
                             'token': self.key,
                             'val': val,
                             'time_stamp': time_stamp.strftime("%Y-%m-%d %H:%M:%S"),
                             'id': id
                          })

    def getEvents(self):
        r = requests.post('http://localhost:5000/events/1',
                          json={
                             'token': self.key,
                          })

        res=json.loads(r.text)
        for item in res['data']:
            print(item['val'],' not in ','[', item['avg']-(3*item['dev']),',',item['avg']+(3*item['dev']),']')

    def createFakeNumberStream(self, N, init_val):

        self.fakeNumberStream.append(init_val)
        for i in range(0, N-1):
            self.fakeNumberStream.append(random.randint(1, 100))

        avg_exp = self.fakeNumberStream[0]
        prev_avg_exp = 0

        sum = 0

        for i, val in enumerate(self.fakeNumberStream):
            sum = sum + val
            prev_avg_exp = avg_exp
            avg_exp = 0.01*val + 0.99*prev_avg_exp



        # sum = 0
        #
        # for i in self.fakeNumberStream:
        #     sum = sum + math.pow(i-self.fakeAvg, 2)

        # self.fakeVar = sum / N - 1
        print('***************************************')
        print('pure data')
        print('---------')
        # print('     entire sample exp avg: ', self.fakeExpAvg)
        print('     exp avg: ', avg_exp)
        print('     normal avg: ', sum/len(self.fakeNumberStream))
        # print('     var: ', self.fakeVar)
        # print('     std_dev: ',math.sqrt(self.fakeVar))
        print('***************************************')
