"""Seed script."""
from os.path import join, dirname
from dotenv import load_dotenv
import sys
from tests.test_stat import test_avg, test_associations, create_env
from tests.test_api import test_api

from app.tasks.update_scripts.iex import iex
from config import App_Param

from app.analytics.mapper import Mapper

from app import db
import app.models

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

if __name__ == '__main__':
    if len(sys.argv) >= 2:
        if sys.argv[1] == "test_api":
            db.drop_all()
            db.create_all()
            test_api()
            db.drop_all()
            db.create_all()

        elif sys.argv[1] == "test_stats":
            db.drop_all()
            db.create_all()
            test_avg()
            db.drop_all()
            db.create_all()

        elif sys.argv[1] == 'test_links':
            db.drop_all()
            db.create_all()
            test_associations()
            db.drop_all()
            db.create_all()

        elif sys.argv[1] == "tests":
            db.drop_all()
            db.create_all()
            test_api()
            test_avg()
            db.drop_all()
            db.create_all()

        elif sys.argv[1] == "seeder":
            iex_seeder = iex(App_Param.TOKEN)
            iex_seeder._init_seed()

        elif sys.argv[1] == "update":
            iex_updater = iex(App_Param.TOKEN)
            iex_updater._run_update()

        elif sys.argv[1] == "gen_env":
            db.drop_all()
            db.create_all()
            create_env(10)

        elif sys.argv[1] == "redo_db":
            db.drop_all()
            db.create_all()

        elif sys.argv[1] == "gen_net":
            mapper = Mapper()
            mapper.save()

        else:
            print('please select an option from:')
            print('\n     test_api')
            print('     test_stats')
            print('     test_links')
            print('     tests')
            print('     seeder')
            print('     update')
            print('     gen_env')
            print('     gen_net')
            print('     redo_db (DESTRUCTIVE) \n')

    else:
        print('please select an option from:')
        print('\n     test_api')
        print('     test_stats')
        print('     test_links')
        print('     tests')
        print('     seeder')
        print('     update')
        print('     gen_env')
        print('     gen_net')
        print('     redo_db (DESTRUCTIVE) \n')
